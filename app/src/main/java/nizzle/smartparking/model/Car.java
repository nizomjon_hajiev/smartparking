package nizzle.smartparking.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 09.08.2016.
 */
public class Car {

    @SerializedName("customer_id")
    private String id;
    @SerializedName("customer_name")
    private String userName;
    @SerializedName("password")
    private String password;
    @SerializedName("car_color")
    private String carColor;
    @SerializedName("customer_phone_number")
    private String phoneNumber;
    @SerializedName("car_number")
    private String carNumber;
    @SerializedName("is_checked")
    private String checked;
    @SerializedName("car_img")
    private String carImage;
    @SerializedName("car_type")
    private String carType;
    @SerializedName("camera_number")
    private String cameraNumber;

    public Car(String id, String userName) {
        this.id = id;
        this.userName = userName;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    public String getCarImage() {
        return carImage;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
    }

    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    public String getCameraNumber() {
        return cameraNumber;
    }

    public void setCameraNumber(String cameraNumber) {
        this.cameraNumber = cameraNumber;
    }
}
