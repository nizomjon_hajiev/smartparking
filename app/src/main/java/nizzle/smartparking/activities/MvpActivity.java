package nizzle.smartparking.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;

import nizzle.smartparking.presenter.BasePresenter;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public abstract class MvpActivity<P extends BasePresenter> extends BaseActivity {

    protected P presenter;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();

    }

    protected abstract P createPresenter();

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
    }
}
