package nizzle.smartparking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.ramotion.foldingcell.FoldingCell;

import java.util.HashSet;
import java.util.List;

import nizzle.smartparking.R;
import nizzle.smartparking.event.CarEvent;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.util.RxBus;

/**
 * Simple example of ListAdapter for using with Folding Cell
 * Adapter holds indexes of unfolded elements for correct work with default reusable views behavior
 */
public class TestAdapter extends ArrayAdapter<CarData> {

    private HashSet<Integer> unfoldedIndexes = new HashSet<>();
    private View.OnClickListener defaultRequestBtnClickListener;
    private RxBus bus;

    public TestAdapter(Context context, List<CarData> objects, RxBus bus)  {
        super(context, 0, objects);
        this.bus  = bus;
    }


    //    getIte
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // get item for selected view
        final CarData item = getItem(position);
        // if cell is exists - reuse it, if not - create the new one from resource
        FoldingCell cell = (FoldingCell) convertView;
        ViewHolder viewHolder;
        if (cell == null) {
            viewHolder = new ViewHolder();
            LayoutInflater vi = LayoutInflater.from(getContext());

            cell = (FoldingCell) vi.inflate(R.layout.cell, parent, false);
            // binding view parts to view holder
            viewHolder.enterTime = (TextView) cell.findViewById(R.id.car_enter_time);
            viewHolder.exitTime = (TextView) cell.findViewById(R.id.car_exit_time);
            viewHolder.totalTime = (TextView) cell.findViewById(R.id.car_total_time);
            viewHolder.carNumbTitle = (TextView) cell.findViewById(R.id.car_number_title);
            viewHolder.carNumb = (TextView) cell.findViewById(R.id.block_area_number);
            viewHolder.carImage = (ImageView) cell.findViewById(R.id.car_image);
            viewHolder.carType = (TextView) cell.findViewById(R.id.floor_number);
            viewHolder.carColor = (TextView) cell.findViewById(R.id.block_area);
            viewHolder.userName = (TextView) cell.findViewById(R.id.user_name);
            viewHolder.userPhone = (TextView) cell.findViewById(R.id.user_phone);
            viewHolder.contentRequestBtn = (TextView) cell.findViewById(R.id.content_request_btn);
            viewHolder.userPhoto = (ImageView) cell.findViewById(R.id.user_photo);
            viewHolder.carColorItem = (TextView) cell.findViewById(R.id.car_color_item);
            viewHolder.carTypeItem = (TextView) cell.findViewById(R.id.car_type_item);
            viewHolder.carNumberItem = (TextView) cell.findViewById(R.id.car_number_item);
            viewHolder.cameraNumber = (TextView) cell.findViewById(R.id.camera_number);


            cell.setTag(viewHolder);
        } else {
            // for existing cell set valid valid state(without animation)
            if (unfoldedIndexes.contains(position)) {
                cell.unfold(true);
            } else {
                cell.fold(true);
            }
            viewHolder = (ViewHolder) cell.getTag();
        }

        // bind data from selected element to view through view holder
        if  (item.getCarNumber()!=null){

            viewHolder.enterTime.setText(item.getEnterTime());
            viewHolder.exitTime.setText(item.getExitTime());
            viewHolder.totalTime.setText(item.getTottalyTimeCalculation());
            viewHolder.carNumbTitle.setText(item.getCarNumber());
            viewHolder.carNumb.setText(item.getCarBlockNumber());
            viewHolder.carColor.setText(item.getCarBlockArea());
            viewHolder.carType.setText(item.getFloorNumber());
            ImageLoader.getInstance().displayImage(item.getCarImage(), viewHolder.carImage);
            viewHolder.userName.setText(item.getUserName());
            viewHolder.userPhone.setText(item.getPhoneNumber());
            ImageLoader.getInstance().displayImage(item.getUserPhoto(), viewHolder.userPhoto);
            viewHolder.carNumberItem.setText(item.getCarNumber());
            viewHolder.carTypeItem.setText(item.getCarType());
            viewHolder.carColorItem.setText(item.getCarColor());
            viewHolder.cameraNumber.setText(item.getCameraNumber());


            if (item.getRequestBtnClickListener() != null) {
                viewHolder.contentRequestBtn.setOnClickListener(item.getRequestBtnClickListener());
            } else {
                // (optionally) add "default" handler if no handler found in item
                viewHolder.contentRequestBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bus.send(CarEvent.call(item));
                    }
                });
            }
            if (item.getRequestImgClickListener() != null) {
                viewHolder.carImage.setOnClickListener(item.getRequestBtnClickListener());
            } else {
                // (optionally) add "default" handler if no handler found in item
                viewHolder.carImage.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        bus.send(CarEvent.singleCar(item));
                    }
                });
            }
        }

        return cell;
    }

    // simple methods for register cell state changes
    public void registerToggle(int position) {
        if (unfoldedIndexes.contains(position))
            registerFold(position);
        else
            registerUnfold(position);
    }

    public void registerFold(int position) {
        unfoldedIndexes.remove(position);
    }

    public void registerUnfold(int position) {
        unfoldedIndexes.add(position);
    }

    public View.OnClickListener getDefaultRequestBtnClickListener() {
        return defaultRequestBtnClickListener;
    }

    public void setDefaultRequestBtnClickListener(View.OnClickListener defaultRequestBtnClickListener) {
        this.defaultRequestBtnClickListener = defaultRequestBtnClickListener;
    }

    // View lookup cache
    private static class ViewHolder {
        TextView enterTime;
        TextView exitTime;
        TextView totalTime;
        TextView carNumbTitle;
        TextView carNumb;
        ImageView carImage;
        TextView carType;
        TextView carColor;
        TextView userName;
        TextView userPhone;
        TextView contentRequestBtn;
        ImageView userPhoto;
        TextView carColorItem;
        TextView carNumberItem;
        TextView carTypeItem;
        TextView cameraNumber;
    }
}