package nizzle.smartparking.view;

import java.util.List;

import nizzle.smartparking.model.Car;
import nizzle.smartparking.model.CarData;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public interface MainView extends BaseView {

    void showCarList(List<CarData> cars);

    void showCarDetails(Car car);

    void showCamera();

    Observable<List<CarData>> getCars();



}
