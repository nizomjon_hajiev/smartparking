package nizzle.smartparking.api;

import android.databinding.BaseObservable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 06.08.2016.
 */
public class SignBody extends BaseObservable {

    @SerializedName("username")
    String username;
    @SerializedName("carcolor")
    String carcolor;
    @SerializedName("phonenum")
    String phonenum;
    @SerializedName("carnumber")
    String carnumber;
    @SerializedName("password")
    String password;
    @SerializedName("checked")
    boolean isChecked;

    public SignBody(String username, String carcolor, String phonenum, String carnumber, String password, boolean isChecked) {
        this.username = username;
        this.carcolor = carcolor;
        this.phonenum = phonenum;
        this.carnumber = carnumber;
        this.password = password;
        this.isChecked = isChecked;
    }

    public String getUsername() {
        return username;
    }

    public String getEmail() {
        return carcolor;
    }

    public String getPhonenumber() {
        return phonenum;
    }

    public String getCarnumber() {
        return carnumber;
    }

    public String getPassword() {
        return password;
    }
}
