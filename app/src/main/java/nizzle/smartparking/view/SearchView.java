package nizzle.smartparking.view;

import java.util.List;

import nizzle.smartparking.model.CarData;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 11.08.2016.
 */
public interface SearchView extends BaseView {

    void showNoResult();

    void hideNoResult();

    void clearSearch();

    void clearResult();

    void search();

    Observable<List<CarData>> getCarDetails(String keyword);

    void onSearchResult(List<CarData> car);

    void finishActivity();
}
