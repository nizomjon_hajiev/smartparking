//package nizzle.smartparking.activities;
//
//import android.databinding.DataBindingUtil;
//import android.graphics.Bitmap;
//import android.graphics.BitmapFactory;
//import android.os.Bundle;
//import android.support.annotation.Nullable;
//import android.util.Base64;
//
//import com.google.gson.Gson;
//import com.google.gson.reflect.TypeToken;
//
//import nizzle.smartparking.R;
//import nizzle.smartparking.databinding.ActivityCarDetailBinding;
//import nizzle.smartparking.model.Car;
//
///**
// * Created by Hajiev Nizomjon Qudrat o'gli on 11.08.2016.
// */
//public class CarDetailActivity extends BaseActivity {
//    Bitmap decodedByte;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        String currentCar = getIntent().getStringExtra("car_body");
//        Gson gson = new Gson();
//        Car mCar = gson.fromJson(currentCar, new TypeToken<Car>() {
//        }.getType());
//        if (mCar.getCarImage() != null) {
//            byte[] decodedString = Base64.decode(mCar.getCarImage(), Base64.DEFAULT);
//            decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
//        }
//        ActivityCarDetailBinding binding = DataBindingUtil.setContentView(this, R.layout.activity_car_detail);
//        binding.setCar(mCar);
//        if (decodedByte != null)
//            binding.imageView1.setImageBitmap(decodedByte);
//    }
//}
