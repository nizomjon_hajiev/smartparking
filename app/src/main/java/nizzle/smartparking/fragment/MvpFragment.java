package nizzle.smartparking.fragment;

import android.os.Bundle;

import nizzle.smartparking.presenter.BasePresenter;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public abstract class MvpFragment<P extends BasePresenter> extends BaseFragment {

    protected P presenter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = createPresenter();

    }

    protected abstract P createPresenter();

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (presenter != null) {
            presenter.onDestroy();
        }
    }
}
