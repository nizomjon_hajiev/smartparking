package nizzle.smartparking.activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.OnClick;
import nizzle.smartparking.R;
import nizzle.smartparking.api.results.SignUpResult;
import nizzle.smartparking.presenter.LoginPresenter;
import nizzle.smartparking.view.LoginView;
import rx.Observable;

/**
 * Created by User on 05.07.2016.
 */

public class LoginActivity extends MvpActivity<LoginPresenter> implements LoginView {

    @BindView(R.id.input_email)
    EditText emailT;
    @BindView(R.id.input_password)
    EditText passwordT;
    @BindView(R.id.checkBox)
    CheckBox mCheckBox;
    @BindView(R.id.btn_login)
    Button loginBtn;

    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
    }

    @Override
    protected LoginPresenter createPresenter() {
        return new LoginPresenter(this);
    }

    @OnClick(R.id.btn_login)
    void loginBtn() {
        login();
    }

    @OnClick(R.id.link_signup)
    void linkToSignUp() {
        navigator.navigateSignUpScreen(LoginActivity.this);
    }

    public void login() {
        String email = emailT.getText().toString();
        String password = passwordT.getText().toString();
        if (!presenter.validate(email, password, null)) {
            onLoginFailed();
        } else {
            presenter.login();
        }
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        loginBtn.setEnabled(true);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Authenticating...");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void setError(String error) {
        Toast.makeText(LoginActivity.this, "Error : " + error, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void navigateToHome() {
        navigator.navigateMainScreen(this);
        finish();
    }

    @Override
    public void onLogin(SignUpResult result) {
        if (result.result.equals("success")) {
            Toast.makeText(LoginActivity.this, result.result, Toast.LENGTH_SHORT).show();
            navigateToHome();
        } else {
            Toast.makeText(LoginActivity.this, result.result, Toast.LENGTH_SHORT).show();
//            navigateToHome();
//            setError(result.result);
        }
    }

    @Override
    public Observable<SignUpResult> postLogin() {
        String email = emailT.getText().toString();
        String password = passwordT.getText().toString();
        return api.login(email, password);
    }
}
