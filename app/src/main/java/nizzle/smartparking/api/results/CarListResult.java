package nizzle.smartparking.api.results;

import java.util.List;

import nizzle.smartparking.model.CarData;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public class CarListResult {

    List<CarData> mCars;

    public List<CarData> getCars() {
        return mCars;
    }

    public void setCars(List<CarData> cars) {
        mCars = cars;
    }
}
