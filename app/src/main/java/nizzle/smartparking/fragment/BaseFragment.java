package nizzle.smartparking.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.LayoutRes;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;

import javax.inject.Inject;

import butterknife.ButterKnife;
import nizzle.smartparking.SMApp;
import nizzle.smartparking.api.Api;
import nizzle.smartparking.navigator.Navigator;
import nizzle.smartparking.util.RxBus;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public abstract class BaseFragment extends Fragment {


    @Inject
    Api api;

    @Inject
    Gson gson;

    @Inject
    Navigator navigator;

    @Inject
    RxBus bus;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        ((SMApp) getActivity().getApplicationContext()).inject(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout(), container, false);
        ButterKnife.bind(this, view);

        return view;
    }

    @LayoutRes
    protected abstract int getLayout();


    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
