package nizzle.smartparking.view;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 05.08.2016.
 */
public interface BaseView {

    void showProgress();

    void hideProgress();

    void setError(String error);

    void showNoNetwork();

}
