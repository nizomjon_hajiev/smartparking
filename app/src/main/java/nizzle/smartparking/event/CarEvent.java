package nizzle.smartparking.event;

import java.util.List;

import nizzle.smartparking.model.CarData;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public class CarEvent {

    private Type type;
    private CarData car;
    private List<CarData> cars;


    public static CarEvent singleCar(CarData car) {
        CarEvent e = new CarEvent();
        e.type = Type.SINGLE_CAR;
        e.car = car;
        return e;
    }

    public static CarEvent listCar(List<CarData> cars) {
        CarEvent e = new CarEvent();
        e.type = Type.LIST_CAR;
        e.cars = cars;
        return e;
    }

    public static CarEvent call(CarData car){
        CarEvent e = new CarEvent();
        e.type = Type.CALLING;
        e.car = car;
        return e;
    }

    public enum Type {
        SINGLE_CAR,
        LIST_CAR,
        CALLING

    }

    public CarData getCar() {
        return car;
    }

    public List<CarData> getCars() {
        return cars;
    }

    public Type getType() {
        return type;
    }
}
