package nizzle.smartparking.activities;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import nizzle.smartparking.R;
import nizzle.smartparking.adapter.CarAdapter;
import nizzle.smartparking.event.CarEvent;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.presenter.SearchPresenter;
import nizzle.smartparking.view.SearchView;
import rx.Observable;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 11.08.2016.
 */
public class SearchActivity extends MvpActivity<SearchPresenter> implements SearchView {

    @BindView(R.id.search_input)
    EditText searchInput;

    @BindView(R.id.no_results_view)
    View noResultsView;
    @BindView(R.id.result_container)
    LinearLayout resultContainer;

    @BindView(R.id.progress_bar_holder)
    View progressBarHolder;

    private boolean isClearVisible = false;
    private Handler mHandler = new Handler();
    CarAdapter mCarAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        setHomeAsUp();



            searchInput.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    search();
                    return true;
                }
                return false;
            }
        });

        searchInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                boolean isEmpty = TextUtils.isEmpty(s);
                if (isEmpty == isClearVisible) {
                    isClearVisible = !isClearVisible;
                    supportInvalidateOptionsMenu();
                }

                if (!isEmpty) {
                    mHandler.removeCallbacks(searchRunnable);
                    mHandler.postDelayed(searchRunnable, 500);
                } else {
                    clearResult();
                }
            }
        });
    }

    @Override
    protected SearchPresenter createPresenter() {
        return new SearchPresenter(this);
    }

    private Runnable searchRunnable = new Runnable() {
        @Override
        public void run() {
            search();
        }
    };


    @OnClick(R.id.result_container)
    void onClickToBack() {
        supportFinishAfterTransition();
    }

    @OnClick(R.id.close_search)
    void closeSearch() {
        clearSearch();
    }


    @Override
    public void showNoResult() {
        clearResult();
        noResultsView.setVisibility(View.VISIBLE);
    }


    @Override
    public void hideNoResult() {

        noResultsView.setVisibility(View.GONE);
    }

    @Override
    public void clearSearch() {
        searchInput.setText("");
        clearResult();
    }

    @Override
    public void clearResult() {
        resultContainer.removeAllViews();
    }

    @Override
    public void search() {

        final String keyword = searchInput.getText().toString().trim();
        if (TextUtils.isEmpty(keyword)) {
            return;
        }
//        RxTextView.textChanges(searchInput)
//                .debounce(400, TimeUnit.MILLISECONDS)
//                .map(CharSequence::toString)
//                .subscribeOn(AndroidSchedulers.mainThread())
//                .observeOn(Schedulers.io())
//                .switchMap(query -> api.search(keyword))
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(new Subscriber<List<CarData>>() {
//                    @Override
//                    public void onCompleted() {
//                  finishActivity();
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Toast.makeText(SearchActivity.this, e.toString(), Toast.LENGTH_SHORT).show();
//                    }
//
//                    @Override
//                    public void onNext(List<CarData> items) {
//                        bus.send(CarEvent.listCar(items));
//                    }
//                });





        presenter.search(keyword);


    }

    @Override
    public Observable<List<CarData>> getCarDetails(String keyword) {
        return api.search(keyword);
    }

    @Override
    public void onSearchResult(List<CarData> cars) {

        bus.send(CarEvent.listCar(cars));

    }

    @Override
    public void finishActivity() {
        finish();
    }

    @Override
    public void showProgress() {
        progressBarHolder.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBarHolder.setVisibility(View.GONE);
    }

    @Override
    public void setError(String error) {
//        List<CarData> cars = new ArrayList<>();
//        cars.add(new CarData("1","A"));
//        cars.add(new CarData("2","B"));
//        cars.add(new CarData("3","C"));
//        bus.send(CarEvent.listCar(cars));


        Toast.makeText(SearchActivity.this, "Error : " + error, Toast.LENGTH_SHORT).show();
    }



//    CarAdapter.onClickListener mOnClickListener = new CarAdapter.onClickListener() {
//        @Override
//        public void click(Car car) {
//
//            navigator.navigateCarDetailsScreen(SearchActivity.this, mGson.toJson(car));
//
//        }
//    };
}
