package nizzle.smartparking.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;

import butterknife.OnClick;
import butterknife.Optional;
import nizzle.smartparking.R;
import nizzle.smartparking.activities.VideoPlayerActivity;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 20.09.2016.
 */
public class MapFragment extends BaseFragment {


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

    }

    @Override
    protected int getLayout() {
        return R.layout.fragment_map;
    }

    @Optional
    @OnClick({R.id.cam1, R.id.cam2, R.id.cam3,R.id.cam4})
    void click(View view) {
        switch (view.getId()) {
            case R.id.cam1: {
                String url = "http://210.119.146.199:8081";
                Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
                intent.putExtra("URL", url);
                startActivity(intent);
                break;
            }
            case R.id.cam2: {
                String url = "http://210.119.146.199//smartparking/camparck/011001_SNHU-B4F-A-001_20150327_153623.jpg";
                Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
                intent.putExtra("URL", url);
                startActivity(intent);
                break;
            }
            case R.id.cam3: {
                String url = "http://210.119.146.199/smartparking/camparck/012014_SNHU-B4F-B-014_20150401_121725.jpg";
                Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
                intent.putExtra("URL", url);
                startActivity(intent);
                break;
            }
            case R.id.cam4: {
                String url = "http://210.119.146.199:8081";
                Intent intent = new Intent(getActivity(), VideoPlayerActivity.class);
                intent.putExtra("URL", url);
                startActivity(intent);
                break;
            }
        }
    }
}
