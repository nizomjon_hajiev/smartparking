package nizzle.smartparking.presenter;

import java.util.List;

import nizzle.smartparking.api.callback.ApiCallback;
import nizzle.smartparking.api.callback.SubscriberCallback;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.view.DurationView;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 05.09.2016.
 */
public class DurationPresenter extends BasePresenter<DurationView> {

    public DurationPresenter(DurationView view) {
        attachView(view);
    }

    public void getCarDetails() {
        mvpView.showProgress();
        unSubscribeAll();
        subscribe(mvpView.getCarDetails(), new SubscriberCallback<>(new ApiCallback<List<CarData>>() {
            @Override
            public void onSuccess(List<CarData> model) {
                mvpView.showListToTable(model);
            }

            @Override
            public void onFailure(int code, String msg) {
                mvpView.setError(msg);
            }

            @Override
            public void onCompleted() {
                mvpView.hideProgress();
            }

            @Override
            public void onNetworkError() {

                mvpView.hideProgress();
                mvpView.showNoNetwork();
            }
        }));
    }
}
