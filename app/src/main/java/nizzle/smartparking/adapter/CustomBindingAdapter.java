package nizzle.smartparking.adapter;

import android.databinding.BindingAdapter;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 12.08.2016.
 */
public class CustomBindingAdapter {

    @BindingAdapter("bind:imageUrl")
    public static void loadImage(ImageView imageView, String url) {
        ImageLoader.getInstance().displayImage(url, imageView);
    }
}
