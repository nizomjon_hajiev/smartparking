package nizzle.smartparking.model;

import android.databinding.BaseObservable;
import android.databinding.Bindable;
import android.view.View;

import com.google.gson.annotations.SerializedName;


/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 12.08.2016.
 */
public class CarData extends BaseObservable {

    @SerializedName("customer_id")
    private String id;
    @SerializedName("customer_name")
    private String userName;
    @SerializedName("customer_img")
    private String userPhoto;
    @SerializedName("password")
    private String password;
    @SerializedName("car_color")
    private String carColor;
    @SerializedName("customer_phone_number")
    private String phoneNumber;
    @SerializedName("car_number")
    private String carNumber;
    @SerializedName("is_checked")
    private String checked;
    @SerializedName("car_img")
    private String carImage;
    @SerializedName("car_type")
    private String carType;
    @SerializedName("camera_number")
    private String cameraNumber;
    @SerializedName("camera_ip_address")
    private String cameraIpAddress;
    @SerializedName("enter_time")
    private String enterTime;
    @SerializedName("exit_time")
    private String exitTime;
    @SerializedName("current_time_staying")
    private String currentTimeStaying;
    @SerializedName("tottaly_time_calculation")
    private String tottalyTimeCalculation;
    @SerializedName("floor_number")
    private String floorNumber;
    @SerializedName("car_block_area")
    private String carBlockArea;
    @SerializedName("car_block_number")
    private String carBlockNumber;
//    "floor_number": "1",
//            "car_block_area": "A",
//            "car_block_number": "1",
    private boolean hasCameraIcon;

    private View.OnClickListener requestBtnClickListener,requestImgClickListener;


    public CarData() {
    }

    public CarData(String userName, String enterTime, String exitTime, String tottalyTimeCalculation) {
        this.userName = userName;
        this.enterTime = enterTime;
        this.exitTime = exitTime;
        this.tottalyTimeCalculation = tottalyTimeCalculation;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Bindable
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Bindable
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Bindable
    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    @Bindable
    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Bindable
    public String getCarNumber() {
        return carNumber;
    }

    public void setCarNumber(String carNumber) {
        this.carNumber = carNumber;
    }

    @Bindable
    public String getChecked() {
        return checked;
    }

    public void setChecked(String checked) {
        this.checked = checked;
    }

    @Bindable
    public String getCarImage() {
        return carImage;
    }

    public void setCarImage(String carImage) {
        this.carImage = carImage;
    }

    @Bindable
    public String getCarType() {
        return carType;
    }

    public void setCarType(String carType) {
        this.carType = carType;
    }

    @Bindable
    public String getCameraNumber() {
        return cameraNumber;
    }

    public void setCameraNumber(String cameraNumber) {
        this.cameraNumber = cameraNumber;
    }

    @Bindable
    public String getCameraIpAddress() {
        return cameraIpAddress;
    }

    public void setCameraIpAddress(String cameraIpAddress) {
        this.cameraIpAddress = cameraIpAddress;
    }

    @Bindable
    public String getEnterTime() {
        return enterTime;
    }

    public void setEnterTime(String enterTime) {
        this.enterTime = enterTime;
    }

    @Bindable
    public String getExitTime() {
        return exitTime;
    }

    public void setExitTime(String exitTime) {
        this.exitTime = exitTime;
    }

    @Bindable
    public String getCurrentTimeStaying() {
        return currentTimeStaying;
    }

    public void setCurrentTimeStaying(String currentTimeStaying) {
        this.currentTimeStaying = currentTimeStaying;
    }

    @Bindable
    public String getTottalyTimeCalculation() {
        return tottalyTimeCalculation;
    }

    public void setTottalyTimeCalculation(String tottalyTimeCalculation) {
        this.tottalyTimeCalculation = tottalyTimeCalculation;
    }

    public String getUserPhoto() {
        return userPhoto;
    }

    public void setUserPhoto(String userPhoto) {
        this.userPhoto = userPhoto;
    }

    @Bindable
    public boolean isHasCameraIcon() {
        return hasCameraIcon;
    }

    public void setHasCameraIcon(boolean hasCameraIcon) {
        this.hasCameraIcon = hasCameraIcon;
    }

    public String getCarBlockArea() {
        return carBlockArea;
    }

    public String getCarBlockNumber() {
        return carBlockNumber;
    }

    public String getFloorNumber() {
        return floorNumber;
    }

    public View.OnClickListener getRequestBtnClickListener() {
        return requestBtnClickListener;
    }

    public void setRequestBtnClickListener(View.OnClickListener requestBtnClickListener) {
        this.requestBtnClickListener = requestBtnClickListener;
    }

    public View.OnClickListener getRequestImgClickListener() {
        return requestImgClickListener;
    }

    public void setRequestImgClickListener(View.OnClickListener requestImgClickListener) {
        this.requestImgClickListener = requestImgClickListener;
    }
}
