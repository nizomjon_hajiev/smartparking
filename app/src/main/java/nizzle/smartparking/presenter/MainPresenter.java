package nizzle.smartparking.presenter;

import java.util.List;

import nizzle.smartparking.api.callback.ApiCallback;
import nizzle.smartparking.api.callback.SubscriberCallback;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.view.MainView;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 27.08.2016.
 */
public class MainPresenter extends BasePresenter<MainView> {


    public MainPresenter(MainView mainView) {
        attachView(mainView);
    }

    public void getCarList() {
        unSubscribeAll();
        mvpView.showProgress();
        subscribe(mvpView.getCars(), new SubscriberCallback<>(new ApiCallback<List<CarData>>() {

            @Override
            public void onSuccess(List<CarData> model) {
                mvpView.showCarList(model);
            }

            @Override
            public void onFailure(int code, String msg) {
                mvpView.hideProgress();
                mvpView.setError(msg);
            }

            @Override
            public void onCompleted() {
                mvpView.hideProgress();
            }

            @Override
            public void onNetworkError() {
                mvpView.showNoNetwork();
            }
        }));
    }


}
