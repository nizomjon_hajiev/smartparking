package nizzle.smartparking.activities;

import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.ByteArrayOutputStream;
import java.io.File;

import butterknife.BindView;
import butterknife.OnClick;
import nizzle.smartparking.R;
import nizzle.smartparking.api.results.SignUpResult;
import nizzle.smartparking.presenter.LoginPresenter;
import nizzle.smartparking.view.LoginView;
import rx.Observable;

/**
 * Created by User on 05.07.2016.
 */
public class SignUpActivity extends PickImageActivity implements LoginView {

    @BindView(R.id.input_name)
    EditText nameT;
    @BindView(R.id.input_phone_number)
    EditText phoneT;
    @BindView(R.id.input_car_number)
    EditText carNumberT;
    @BindView(R.id.input_password)
    EditText passwordT;
    @BindView(R.id.btn_signup)
    Button signBtn;
    @BindView(R.id.user_photo)
    ImageView userPhoto;

    LoginPresenter mLoginPresenter;
    ProgressDialog progressDialog;
    String imagePath;
    byte[] imageArray;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        mLoginPresenter = new LoginPresenter(this);
    }

    @OnClick(R.id.btn_signup)
    void signBtn() {
        sign();
    }

    @OnClick(R.id.link_login)
    void linkToLogin() {
        navigator.navigateLoginScreen(SignUpActivity.this);
    }

    private void sign() {


//        if (!mLoginPresenter.validate(email, password, name)) {
//            onLoginFailed();
//        } else {
        mLoginPresenter.login();
//        }
    }

    public void onLoginFailed() {
        Toast.makeText(getBaseContext(), "Login failed", Toast.LENGTH_LONG).show();
        signBtn.setEnabled(true);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(SignUpActivity.this);
        progressDialog.setIndeterminate(true);
        progressDialog.setMessage("Creating Account...");
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        progressDialog.dismiss();
    }

    @Override
    public void navigateToHome() {
        finish();
    }

    @Override
    public void onLogin(SignUpResult result) {
        if (result.result.equals("Register success")) {
            navigateToHome();
        } else {
            setError(result.result);
        }
    }

    @OnClick(R.id.user_photo)
    void userPhoto() {
        pickImage(REQUEST_PICK_PROFILE_IMAGE);
    }

    @Override
    public Observable<SignUpResult> postLogin() {
        String username = nameT.getText().toString();
        String phoneNumber = phoneT.getText().toString();
        String carNumber = carNumberT.getText().toString();
        String password = passwordT.getText().toString();
        Bitmap src = BitmapFactory.decodeFile(imagePath);
        String test = getStringImage(src);
        return api.register(username, password,  phoneNumber, Integer.valueOf(carNumber),  test);
    }

    @Override
    public void setError(String message) {
        Toast.makeText(SignUpActivity.this, "Error :  " + message, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onPickedImageReady(String filePath) {
        imagePath = filePath;
        String uri = String.valueOf(Uri.fromFile(new File(filePath)));
        ImageLoader.getInstance().displayImage(uri, userPhoto);
    }

    public String getStringImage(Bitmap bmp) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        bmp.compress(Bitmap.CompressFormat.JPEG, 100, baos);
        byte[] imageBytes = baos.toByteArray();
        String encodedImage = Base64.encodeToString(imageBytes, Base64.DEFAULT);
        return encodedImage;
    }
}
