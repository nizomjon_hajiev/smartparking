package nizzle.smartparking.presenter;

import java.util.List;

import nizzle.smartparking.api.callback.ApiCallback;
import nizzle.smartparking.api.callback.SubscriberCallback;
import nizzle.smartparking.model.CarData;
import nizzle.smartparking.view.CameraView;

/**
 * Created by Hajiev Nizomjon Qudrat o'gli on 03.09.2016.
 */
public class CameraPresenter extends BasePresenter<CameraView> {

    public CameraPresenter(CameraView view) {
        attachView(view);
    }

    public void getCarList() {
        mvpView.showProgress();
        unSubscribeAll();
        subscribe(mvpView.getCarDetails(), new SubscriberCallback<>(new ApiCallback<List<CarData>>() {
            @Override
            public void onSuccess(List<CarData> model) {
                mvpView.showCarList(model);
            }

            @Override
            public void onFailure(int code, String msg) {
                mvpView.setError(msg);
            }

            @Override
            public void onCompleted() {
                mvpView.hideProgress();

            }

            @Override
            public void onNetworkError() {
                mvpView.hideProgress();
                mvpView.showNoNetwork();
            }
        }));
    }

}
